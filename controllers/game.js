const Game = require('../models/game');

exports.createGame = (req, res) => {
    const game = new Game({
        code: req.params.code
    });

    game.save().then(() => {
        res.status(200).json(game);
    }).catch((error) => {
        res.status(400).json({error: error});
    });
};

exports.getGames = (req, res) => {
    Game.find().then((games) => {
        res.status(200).json(games);
    }).catch((error) => {
        res.stat(400).json({error: error});
    })
};

exports.getGame = (req, res) => {
    Game.findOne({
        code: req.params.code
    }).then((game) => {
        res.status(200).json(game);
    }).catch((error) => {
        res.status(400).json({error: error});
    })
};
