const express = require('express');
const router = express.Router();

const gameCtrl = require('../controllers/game');

router.post('/:code', gameCtrl.createGame);
router.get('/:code', gameCtrl.getGame);
router.get('/games', gameCtrl.getGames);

module.exports = router;
