const express = require('express');
const router = express.Router();

const answersCtrl = require('../controllers/answers');

module.exports = router;
