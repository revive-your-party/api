const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


const gameSchema = mongoose.Schema({
    code: {type: String, require: true, unique: true},
    players: {type: Array}
});

gameSchema.plugin(uniqueValidator);

module.exports = mongoose.model('Game', gameSchema);
